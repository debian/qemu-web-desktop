#! /usr/bin/make -f

# qemu-web-desktop project Makefile
# This is not the one you search for.
#
# Use: cd src; sudo make install

VERSION=$(shell awk -F= '/config{version}[[:space:]]*=/ {print $$2}' src/cgi-bin/qemu-web-desktop.pl | sed -e 's/;.*//' -e 's/"//g' -e 's/[[:space:]]*//g' | tail -1 )

dist:
	tar --create \
		--auto-compress \
		--file=../qemu-web-desktop-$(VERSION).tar.gz \
		--transform=s,^./,qemu-web-desktop-$(VERSION)/, \
		--exclude=./src/html/desktop/machines \
		--exclude=./.git* \
		--exclude=*~ \
		--exclude=./debian \
		.
deb:
	@echo " Building Debian package"
	# requires: git-buildpackage dh-apache2 dh-sysuser devscripts
	-rm -rf debian build
	cp -r packaging/debian .
	# remove too big ISO
	mv src/html/desktop/machines/slax.iso .
	debuild -b || true
	rm -rf debian build
	mv slax.iso src/html/desktop/machines/slax.iso
	@echo "[OK] Build deb"
