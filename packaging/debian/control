Source: qemu-web-desktop
Section: science
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Roland Mas <lolando@debian.org>, Emmanuel Farhi <emmanuel.farhi@synchrotron-soleil.fr>, Frédéric-Emmanuel Picca <picca@debian.org>
Build-Depends: debhelper (>= 12), dh-apache2, pandoc
Standards-Version: 4.1.2
Vcs-Browser: https://salsa.debian.org/debian/qemu-web-desktop
Vcs-Git: https://salsa.debian.org/debian/qemu-web-desktop.git
Homepage: https://gitlab.com/soleil-data-treatment/soleil-software-projects/remote-desktop

Package: qemu-web-desktop
Architecture: amd64 ppc64el arm64 riscv64 loong64
Depends: ${shlibs:Depends}, ${misc:Depends},
  apache2,
  libapache2-mod-perl2,
  libapache2-mpm-itk,
  websockify,
  qemu-system-x86 [amd64],
  qemu-system-arm [arm64],
  qemu-system-ppc [ppc64el],
  qemu-system-misc [loong64],
  bridge-utils,
  iptables,
  dnsmasq,
  guestfs-tools,
  libcgi-pm-perl,
  liblist-moreutils-perl,
  libsys-cpu-perl,
  libsys-cpuload-perl,
  libsys-meminfo-perl,
  libnet-dns-perl,
  libproc-background-perl,
  libproc-processtable-perl,
  libemail-valid-perl,
  libnet-smtps-perl,
  libmail-imapclient-perl,
  libnet-ldap-perl,
  libemail-valid-perl,
  libjson-perl,
  libtext-qrcode-perl,
  novnc,
  confget,
  adduser
Recommends: ${misc:Recommends}
Suggests: qemu-system
Description: Remote desktop service with virtual machines in a browser (DARTS).
 A remote desktop service that launches virtual machines and displays
 them in your browser. Place virtual machine files (ISO, QCOW2, VDI,
 VMDK...) into /var/lib/qemu-web-desktop/machines, add their name in
 the /etc/qemu-web-desktop/machines.conf file, and run qwdctl
 refresh. You can tune the service settings in the
 /etc/qemu-web-desktop/config.pl
 This project is also named Data Analysis Remote Treatment Service (DARTS).
 .
 Once installed, connect to http://server/qemu-web-desktop
