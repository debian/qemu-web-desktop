# qemu-web-desktop: Installation 

<br>
<img src="src/html/desktop/images/darts_logo.png" height=200>
<br>

This documentation details how to install the qemu-web-desktop/DARTS.
The installation steps for further GPU pass-through are described in [GPU.md](GPU.md).
To configure the service, refer to [CONFIGURE.md](CONFIGURE.md).

Table of contents:

1. [Installation: The short story](#installation-the-short-story)
2. [Manual installation](#manual-installation)

## Installation: The short story

| System                   | Install       |
|--------------------------|---------------|
| Debian/Ubuntu/Mint       | `sudo -E apt install qemu-web-desktop`      | 
| Arch/Endeavour/Manjaro   | `sudo -E yay -S qemu-web-desktop`  (in AUR) |
| RedHat/Fedora            | Use below method. :warning: `qwdctl start VM` works, but the web service is broken |
| Manual Install           | git clone https://gitlab.com/soleil-data-treatment/soleil-software-projects/qemu-web-desktop; <br>`cd qemu-web-desktop/src`; <br>`sudo make deps`; <br>`sudo make install`; |

Then, if you are in a hurry:

1. Edit the machine list file with command `sudo -E qwdctl edit machines` and un-comment one of the entries (e.g. the Slax one, or TinyCore). 
1. Open the URL http://localhost/qemu-web-desktop/, enter a fake user ID (the authentication is inactivated at start), and click on Create. A VM should start.

Or proceed with the [CONFIGURE.md](CONFIGURE.md) notes. If you wish to use a GPU, have a look at [GPU.md](GPU.md).


## Manual installation

Get the source code (or a release):
- git clone https://gitlab.com/soleil-data-treatment/soleil-software-projects/qemu-web-desktop
- you may alternatively get a release.
- :warning: if you just cloned the repository, make sure you collect the src/html/desktop/machines directory with [LFS](https://git-lfs.github.com/). Install it with `sudo apt install git-lfs` then, from the repo, `git-lfs install; git lfs pull`.
- `cd qemu-web-desktop/src`

Dependencies:

- **Debian-class** systems (Ubuntu/Mint...): `apache2 libapache2-mod-perl2 novnc websockify qemu-kvm bridge-utils qemu iptables dnsmasq libcgi-pm-perl liblist-moreutils-perl libsys-cpu-perl libsys-cpuload-perl libsys-meminfo-perl libnet-dns-perl confget libproc-background-perl libproc-processtable-perl libemail-valid-perl  libnet-smtps-perl libmail-imapclient-perl libnet-ldap-perl libemail-valid-perl libjson-perl libwww-perl libguestfs-tools libapache2-mpm-itk libtext-qrcode-perl libnet-ssh2-perl`
- **Arch-class** systems (Endeavour, Manjaro, Cauchy): `python-numpy python-setuptools apache mod_itk qemu-desktop bridge-utils qemu-img dnsmasq guestfs-tools perl-cgi perl-list-moreutils perl-sys-cpu perl-sys-meminfo perl-net-dns perl-proc-processtable perl-mail-imapclient perl-ldap perl-json perl-libwww mod_perl novnc websockify perl-proc-background perl-email-valid perl-net-smtps perl-text-qrcode perl-net-ssh2 confget`
- **Fedora/RedHat-class** systems: `mod_perl novnc python3-websockify crudini guestfs-tools perl-CGI perl-List-MoreUtils perl-Sys-CPU perl-Sys-MemInfo perl-Net-DNS perl-Proc-Simple perl-Proc-ProcessTable perl-Email-Valid perl-Net-SMTPS perl-Mail-IMAPClient perl-LDAP perl-JSON perl-libwww-perl perl-Text-QRCode perl-Net-SSH2 perl-CPAN`

These dependencies are installed with:

-  `sudo make deps`.

Then install QWD with:

- `sudo make install`

In case the `make` command fails, have a look at the Makefile...

:warning: In case you manually install the software, and target directories are customized, make sure to edit the `qwdctl` file to change the filepath to match configuration files (at start).

To un-install, just do a:

- `cd src; sudo make uninstall`.

We have prepared two videos that demonstrate the semi-automatic procedure (Debian).

[![DARTS Installation and configuration](paper/darts-install.png)](https://drive.google.com/file/d/11_t28Z3FyjXDqtCmHf2XMezbjKC89ch8/view?usp=drive_link "DARTS Installation and configuration")
[![DARTS Usage](paper/darts-usage.png)](https://drive.google.com/file/d/1RKQSW5vSIWFbyKSe5D0DnqulWMzi2_yE/view?usp=drive_link "DARTS Usage")

## Building a Debian package

To build a Debian package out of this repository or https://salsa.debian.org/debian/qemu-web-desktop, use:
```
sudo apt install git-buildpackage dh-apache2 dh-sysuser devscripts help2man
make deb
sudo apt install ../qemu-web-desktop_*_amd64.deb
# uncomment e.g. [slax] and [tinycore] entries in /etc/qemu-web-desktop except for 'url' lines.
sudo qwdctl refresh
```
will create a `.deb` package in the directory level above.

## Building an Arch package

```
cd packaging/arch
makepkg -s
# install
pacman -U ./qemu-web-desktop*.zst
```

